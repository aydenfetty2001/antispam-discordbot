package de.flori4nk.antispambot.log;

import de.flori4nk.antispambot.BotUtils;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IGuild;

import java.util.List;

public class Logger {

    public static void announceLogon(IGuild g) {
        Logger.sendLogMessage(g, "Client-Login: System is active now");
    }

    public static void announceLogoff(IGuild g) {
        Logger.sendLogMessage(g, "Entering Maintenance Mode...");
        Logger.sendLogMessage(g, "Client-Logout: System is no longer active");
    }

    public static void sendLogMessage(IGuild g, String s) {
        List<IChannel> lst = g.getChannelsByName("antispam-logs");
        if(lst.isEmpty()) {
            g.createChannel("antispam-logs");
            for(IChannel c : g.getChannelsByName("antispam-logs")) {
                BotUtils.sendMessage(c, s);
            }
        }else {
            for(IChannel c : g.getChannelsByName("antispam-logs")) {
                BotUtils.sendMessage(c, s);
            }

        }

    }

}
