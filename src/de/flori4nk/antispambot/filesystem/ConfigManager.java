package de.flori4nk.antispambot.filesystem;

import de.flori4nk.antispambot.conditions.Condition;

import java.io.*;
import java.util.ArrayList;

public class ConfigManager {

    private static ArrayList<Condition> loadedConditions = new ArrayList<>();
    private final File configDirectory = new File(System.getProperty("user.dir") + "/config");
    private final File customConditions = new File(configDirectory.getAbsolutePath()+"/conditions.cfg");

    public void init() {
        if(!configDirectory.exists()) {
                configDirectory.mkdir();
        }
        if(!customConditions.exists()) {
            try {
                customConditions.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void load() {
        try {
            BufferedReader br = new BufferedReader(new FileReader(customConditions));
            String l;
            while((l = br.readLine()) != null) {
                if(l.startsWith("#")) continue;
                String[] splitted = l.split("==");
                if(splitted.length != 2) {
                    System.err.println("Please check your cfg file");
                    System.exit(1);
                }
                String regex = splitted[0];
                String id = splitted[1];

                loadedConditions.add(new Condition(id, regex));
                System.out.println("[L] " + id + " " + regex);
            }
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static ArrayList<Condition> getLoadedConditions() {
        return loadedConditions;
    }
}
