package de.flori4nk.antispambot.event;

import sx.blah.discord.api.events.EventSubscriber;
import sx.blah.discord.handle.impl.events.guild.GuildCreateEvent;
import sx.blah.discord.handle.obj.IGuild;

import java.util.ArrayList;

public class CreateGuildListener {
    public static boolean fired = false;
    static ArrayList<IGuild> guilds = new ArrayList<>();

    @EventSubscriber
    public void onCreate(GuildCreateEvent e) {
        guilds.add(e.getGuild());
        CreateGuildListener.fired = true;
    }

}
