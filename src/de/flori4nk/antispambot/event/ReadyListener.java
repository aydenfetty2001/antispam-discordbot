package de.flori4nk.antispambot.event;

import de.flori4nk.antispambot.log.Logger;
import sx.blah.discord.api.events.EventSubscriber;
import sx.blah.discord.handle.impl.events.ReadyEvent;
import sx.blah.discord.handle.obj.IGuild;

import java.util.ArrayList;

public class ReadyListener {

    private ArrayList<IGuild> announced = new ArrayList<>();

    @EventSubscriber
    public void isReady(ReadyEvent e) {
        System.out.print("Announcing Startup...");
        while (true) {
            if(CreateGuildListener.guilds.isEmpty()) return;
            for (IGuild g : CreateGuildListener.guilds) {
                if(announced.contains(g)) return;
                Logger.announceLogon(g);
                announced.add(g);
            }
        }
    }

}
