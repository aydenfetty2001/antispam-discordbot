package de.flori4nk.antispambot.event;

import de.flori4nk.antispambot.BotUtils;
import de.flori4nk.antispambot.log.Logger;
import sx.blah.discord.api.events.EventSubscriber;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.handle.obj.IRole;

public class MessageReceivedListener {


    @EventSubscriber
    public void onMessage(MessageReceivedEvent event) {
        if(event.getMessage().getContent().equalsIgnoreCase(BotUtils.prefix + "toggle")) {

            for(IRole r : event.getAuthor().getRolesForGuild(event.getGuild())) {
                if(r.getName().equals("AntiSpam-Control")) {
                    if(BotUtils.active) {
                        BotUtils.active = false;
                        BotUtils.sendMessage(event.getChannel(), "Deactivated!");
                        Logger.sendLogMessage(event.getGuild(), "`" + event.getAuthor().getName() + "#" + event.getAuthor().getDiscriminator() + "` deactivated the AntiSpam-System");
                    }else{
                        BotUtils.active = true;
                        BotUtils.sendMessage(event.getChannel(), "Activated!");
                        Logger.sendLogMessage(event.getGuild(), "`" + event.getAuthor().getName() + "#" + event.getAuthor().getDiscriminator() + "` activated the AntiSpam-System");
                    }
                    return;
                }
            }
            BotUtils.sendMessage(event.getChannel(), "You aren't permitted to do that! You Need the `AntiSpam-Control` role. The attempt has been logged.");
            Logger.sendLogMessage(event.getGuild(),  "`" + event.getAuthor().getName() + "#" + event.getAuthor().getDiscriminator() + "` tried to toggle the AntiSpam-System.");
        }
        if(event.getMessage().getContent().equalsIgnoreCase(BotUtils.prefix + "lockdown")) {

            for(IRole r : event.getAuthor().getRolesForGuild(event.getGuild())) {
                if(r.getName().equals("AntiSpam-Control")) {
                    if(BotUtils.lockdown) {
                        BotUtils.lockdown = false;
                        BotUtils.sendMessage(event.getChannel(), "Deactivated!");
                        Logger.sendLogMessage(event.getGuild(), "`" + event.getAuthor().getName() + "#" + event.getAuthor().getDiscriminator() + "` reopened the Server.");
                    }else{
                        BotUtils.lockdown = true;
                        BotUtils.sendMessage(event.getChannel(), "Activated!");
                        Logger.sendLogMessage(event.getGuild(), "`" + event.getAuthor().getName() + "#" + event.getAuthor().getDiscriminator() + "` initiated a Lockdown!");
                    }
                    return;
                }
            }
            BotUtils.sendMessage(event.getChannel(), "You aren't permitted to do that! You Need the `AntiSpam-Control` role. This attempt has been logged.");
            Logger.sendLogMessage(event.getGuild(),  "`" + event.getAuthor().getName() + "#" + event.getAuthor().getDiscriminator() + "` tried to initiate a Lockdown.");
        }
    }

}
