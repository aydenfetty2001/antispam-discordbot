package de.flori4nk.antispambot.event;

import de.flori4nk.antispambot.BotUtils;
import de.flori4nk.antispambot.filesystem.ConfigManager;
import de.flori4nk.antispambot.log.Logger;
import de.flori4nk.antispambot.conditions.Condition;
import sx.blah.discord.api.events.EventSubscriber;
import sx.blah.discord.handle.impl.events.guild.member.UserJoinEvent;
import sx.blah.discord.util.DiscordException;
import sx.blah.discord.util.RequestBuffer;

import java.util.regex.Pattern;

public class JoinEventListener {

    @EventSubscriber
    public void onJoin(UserJoinEvent event){
        // Spam Patterns
        for(Condition bannable : ConfigManager.getLoadedConditions()) {
            if(Pattern.matches(bannable.getRegEx(), event.getUser().getName())) {
                if(!BotUtils.active) {
                    Logger.sendLogMessage(event.getGuild(), "Bot detected, but not banned: `" + event.getUser().getName() + "#" + event.getUser().getDiscriminator() + "`");
                    return;
                }
                RequestBuffer.request(() -> {
                    try{
                        event.getGuild().banUser(event.getUser(), "AntiSpam-System: Bot detected; Pattern: " + bannable.getID());
                        Logger.sendLogMessage(event.getGuild(), "Bot detected: `" + event.getUser().getName()
                                                + "#" + event.getUser().getDiscriminator() + "` (" + bannable.getID() + ")");
                    } catch (DiscordException e){
                        e.printStackTrace();
                    }
                });
            }
        }

        // Lockdown
        if(BotUtils.lockdown) {
            RequestBuffer.request(() -> {
                try{
                    event.getGuild().kickUser(event.getUser(), "AntiSpam-System: Lockdown");
                    Logger.sendLogMessage(event.getGuild(), "Lockdown-Kick: `" + event.getUser().getName() + "#" + event.getUser().getDiscriminator() + "`");
                } catch (DiscordException e){
                    e.printStackTrace();
                }
            });
        }

    }

}
