package de.flori4nk.antispambot;

import sx.blah.discord.api.ClientBuilder;
import sx.blah.discord.api.IDiscordClient;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IPrivateChannel;
import sx.blah.discord.handle.obj.IUser;
import sx.blah.discord.util.DiscordException;
import sx.blah.discord.util.RequestBuffer;

public class BotUtils {

    public static boolean active = true;
    public static boolean lockdown = false;


    public static final String prefix = "antispam ";

    static IDiscordClient getBuiltDiscordClient(String token){
        return new ClientBuilder()
                .withToken(token)
                .build();
    }

    public static void sendMessage(IChannel channel, String message){
        RequestBuffer.request(() -> {
            try{
                channel.sendMessage(message);
            } catch (DiscordException e){
                e.printStackTrace();
            }
        });
    }

    public static void sendPM(IUser user, String message) {
        RequestBuffer.request(() -> {
            try{
                IPrivateChannel ch = user.getOrCreatePMChannel();
                ch.sendMessage(message);
            } catch (DiscordException e){
                e.printStackTrace();
            }
        });
    }


    public static void sendPM_nrb(IUser user, String message) { // _nrb = No RequestBuffer
        IPrivateChannel ch = user.getOrCreatePMChannel();
        ch.sendMessage(message);
    }
}