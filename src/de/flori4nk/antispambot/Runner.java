package de.flori4nk.antispambot;

import de.flori4nk.antispambot.event.CreateGuildListener;
import de.flori4nk.antispambot.event.JoinEventListener;
import de.flori4nk.antispambot.event.MessageReceivedListener;
import de.flori4nk.antispambot.event.ReadyListener;
import de.flori4nk.antispambot.filesystem.ConfigManager;
import de.flori4nk.antispambot.log.Logger;
import sx.blah.discord.api.IDiscordClient;
import sx.blah.discord.handle.obj.ActivityType;
import sx.blah.discord.handle.obj.IGuild;
import sx.blah.discord.handle.obj.StatusType;

import java.util.Scanner;

class Runner {

    private static IDiscordClient cli;

    public static void main(String[] args){

        if(args.length != 1){
            System.out.println("Syntax: <bot_token>");
            return;
        }

        ConfigManager configManager = new ConfigManager();
        configManager.init();
        configManager.load();

        cli = BotUtils.getBuiltDiscordClient(args[0]);

        cli.getDispatcher().registerListener(new JoinEventListener());
        cli.getDispatcher().registerListener(new CreateGuildListener());
        cli.getDispatcher().registerListener(new MessageReceivedListener());
        cli.getDispatcher().registerListener(new ReadyListener());


        cli.login();
        while(!cli.isLoggedIn() || !cli.isReady()) {
            //wait
        }
        System.out.println("Logged in.");
        cli.changePresence(StatusType.IDLE, ActivityType.WATCHING, "flori4nk.de/bots");
        if (!CreateGuildListener.fired) {
            Scanner s = new Scanner(System.in);
            System.out.println("Enter anything to exit");
            String line = s.nextLine();
            for(IGuild g : cli.getGuilds()) {
                Logger.announceLogoff(g);
            }
            System.exit(0);
        }
    }

}
