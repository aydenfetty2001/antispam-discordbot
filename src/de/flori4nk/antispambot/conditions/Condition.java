package de.flori4nk.antispambot.conditions;

import java.util.ArrayList;

public class Condition {
    private String regex;
    private String id;

    public Condition(String id, String regex) {
        this.id = id;
        this.regex = regex;
    }

    public String getID() {
        return id;
    }

    public String getRegEx() {
        return regex;
    }

}
