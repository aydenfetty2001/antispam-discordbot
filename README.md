# antispam-discordbot
A Discord-Bot written in Java using Discord4J, that automatically detects and bans Spambots

Please consider donating.


[![Donate](https://img.shields.io/badge/Donate-Patreon-orange.svg)](https://patreon.com/flori4nk)



### Current Features
- Logging in #antispam-logs text channel
- Automatic detection of invite-links in names
- Auto-Ban
- Server Lockdown
- Temporary toggling of system.
- Automatic detection of patterns that are often used by spam bots.

### Planned Features
- Filter Chat Messages

### Screenshots
- Bot detected (message in #antispam-logs)

![Bot detected](/img/screenshot_botdetected.png?raw=true)

- In the 'Ban' menu of Discord

![Ban reason](/img/screenshot_botban.png?raw=true)

```
Copyright: © Flori4nK, http://Flori4nK.de
```

### Dependencies:
Discord4J:
https://github.com/Discord4J/Discord4J

